﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using ClosedXML.Excel;

namespace SerialPortStream
{
    public partial class Form1 : Form
    {
        static bool _continue;
        static SerialPort _serialPort;
        private Thread _readThread;
        public Form1()
        {
            InitializeComponent();
            foreach (string s in SerialPort.GetPortNames())
            {
                comboPortName.Items.Add(s);
            }
            _serialPort = new SerialPort();
            //comboPortName.SelectedIndex = 0;
            comboBaudRate.SelectedIndex = 0;
            comboDataBits.SelectedIndex = 0;
            comboHandshake.SelectedIndex = 0;
            comboParity.SelectedIndex = 0;
            comboStopBits.SelectedIndex = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var workBook = new XLWorkbook();
            var workSheet = workBook.Worksheets.Add("Raw Data");
            workSheet.Cell("A1").Value = "Hello World!";
            progressBar1.Maximum = richTextBoxRead.Lines.Length;
            foreach (var r in Enumerable.Range(1, richTextBoxRead.Lines.Length))
            {
                workSheet.Cell("A" + r).Value = richTextBoxRead.Lines[r - 1];
                progressBar1.Value = r;
            }
        
                //for (int index = 1; index < richTextBoxRead.Lines.Length; index++)
                //{
                //    workSheet.Cell("A1").Value = richTextBoxRead.Lines[index];
                //}
                workBook.SaveAs("C:/Temp/rawData.xlsx");
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            {
                _serialPort.PortName = comboPortName.SelectedItem.ToString();
                _serialPort.BaudRate = Convert.ToInt32(comboBaudRate.SelectedItem);
                _serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), comboParity.SelectedItem.ToString(), true);
                _serialPort.DataBits = Convert.ToInt32(comboDataBits.SelectedItem);
                _serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), comboStopBits.SelectedItem.ToString(), true);
                _serialPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), comboHandshake.SelectedItem.ToString(), true);

                _serialPort.ReadTimeout = 500;
                _serialPort.WriteTimeout = 500;
            }

            _serialPort.Open();
            if(_serialPort.IsOpen)
            {
                buttonPortOpen.Enabled = false;
                buttonStartRead.Enabled = true;
                buttonPortOpen.BackColor = Color.Green;
            }
            else
            {
                buttonPortOpen.BackColor = Color.Red;
            }
            
        }

        private void buttonStartRead_Click(object sender, EventArgs e)
        {
            buttonStartRead.Enabled = false;
            buttonStopRead.Enabled = true;
            _readThread = new Thread(Read);
            _continue = true;
            _readThread.Start();
        }
        private delegate void SafeCallDelegate(string text);

        private void Read()
        {
            while (_continue)
            {
                if (buttonStartRead.Enabled)
                {
                    _continue = false;
                }
                else if(richTextBoxRead.InvokeRequired)
                {
                    richTextBoxRead.Invoke(new Action(() => richTextBoxRead.AppendText(_serialPort.ReadLine())));
                }
                Thread.Sleep(1);
            }
            _readThread.Join();
            _serialPort.Close();
        }

        private void buttonStopRead_Click(object sender, EventArgs e)
        {
            buttonStartRead.Enabled = true;
            buttonStopRead.Enabled = false;
            buttonExportExcel.Enabled = true;
        }
    }
}
